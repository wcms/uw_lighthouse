This repository contains configuration for doing Lighthouse testing 
including performance budget testing for the WCMS. Follow these 
instructions to test your own sites.

These instructions have been tested on Vagrant and may need to be adjusted for
use elsewhere.

```
# Install chromium-browser.
sudo apt-get install chromium-browser
# Chromium 83+ required.
chromium-browser --version

# Check versions.
# Nodejs 12+ required.
node --version
# Npm 6.14+ required.
npm --version

# Install Lighthouse.
sudo npm install -g lighthouse

# These instructions are written for Lighthouse 6.0.
lighthouse --version

# Checkout the UW Lighthouse configuration.
cd /var/www/html
git clone ist-git@git.uwaterloo.ca:wcms/uw_lighthouse.git
cd uw_lighthouse

# Run Lighthouse using the config file. Replace "URL" with the site you 
# want to test. This can be on your VM or any other URL.
lighthouse --cli-flags-path=./cli-flags.json URL
```

Lighthouse will generate a report named like: 
DOMAIN_DATE_TIME.report.html. These can be viewed at:

https://wcms-vagrant/uw_lighthouse/
